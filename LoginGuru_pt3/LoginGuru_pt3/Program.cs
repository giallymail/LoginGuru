﻿using System;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
namespace LoginGuru_pt3
{
    class Program
    {
        private static IWebDriver Driver;

        static void Main(string[] args)
        {
            Driver = new ChromeDriver(Variables.WebDriverPath);
            Driver.Manage().Window.Maximize();

            // Tasks
            login();
            thirdQuest();

            try
            {
                deleteElement();
                newElement();
                editElement();
            }
            catch
            {
                Console.WriteLine("Can't find any element!");
            
                newElement();
                newElement();
                newElement();
                editElement();
                deleteElement();
            }

            Console.ReadLine();
        }
        static void login()
        {
            Driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");

            // Define elements in webpage
            var Username = Driver.FindElement(By.Name("txtUser"));
            var Password = Driver.FindElement(By.Name("txtPass"));
            var Btn = Driver.FindElement(By.Name("btnLogin"));

            // Input text, password and click login
            Username.SendKeys("g.suriani");
            Password.SendKeys("Smartpeg20");
            Btn.Click();
        }

        static void thirdQuest()
        {
            //                                           Da rimuovere, Presente nel file originale                                                                      //

            clickOn(By.Id("ctl00_cphGuruMaster_272"));
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit"));
            Thread.Sleep(Variables.defaultTimeout);
            //ClearOn(By.Id("ctl00_cphGuruMaster_txtTelefono"));
            //typeOn(By.Id("ctl00_cphGuruMaster_txtTelefono"), "123 456 7890");
            //new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaeseRecapito"))).SelectByIndex(new Random().Next(1, 12));
            //clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));

            //                                          Da rimuovere, Presente nel file originale                                                                       //



            IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[4]/span/span/input"));
        }

        static void newElement()
        {
            int date = new Random().Next(1, 31);
            int month = new Random().Next(6, 12);
            int year = new Random().Next(2021, 2070);
            int causale = new Random().Next(1, 4);
            var valore = new Random().Next(1, 100);
            Thread.Sleep(Variables.defaultTimeout);
                        
                clickOn(By.XPath("(//table[@class='Tb']//input[@value='Nuovo'][@type='button'])[1]"));
                        
            Thread.Sleep(Variables.defaultTimeout);
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoAltraEsperienza"))).SelectByIndex(causale);
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_txtDataInizioAltraEsperienza"), "27/06/2007");
            typeOn(By.Id("ctl00_cphGuruMaster_txtDataFineAltraEsperienza"), date + "/" + month + "/" + year);
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlLuogo"))).SelectByIndex(valore);
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
            Thread.Sleep(Variables.defaultTimeout);

        }

        static void editElement()
        {
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div[3]/div/fieldset/div[2]/div/div/table/tbody/tr[2]/td[1]/input"));
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_txtNote"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
        }

        static void deleteElement()
        {
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div[3]/div/fieldset/div[2]/div/div/table/tbody/tr[2]/td[1]/input"));
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_btnElimina"));
            IAlert alert = Driver.SwitchTo().Alert();
            alert.Accept();
        }




        // Utils


        // Random string
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        static void clickOn(By args)
        {
            Driver.FindElement(args).Click();
        }

        static void typeOn(By args, string typeElement)
        {
            Driver.FindElement(args).SendKeys(typeElement);
        }

        static void ClearOn(By args)
        {
            Driver.FindElement(args).Clear();
        }

    }

}
