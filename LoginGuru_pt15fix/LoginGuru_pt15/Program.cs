﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace gurutest_ptCore
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(Variables.WebDriverPath);
            driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");
            driver.Manage().Window.Maximize();
            //login
            IWebElement username = driver.FindElement(By.Id("txtUser"));
            username.SendKeys("f.chioccoloni");
            IWebElement pass = driver.FindElement(By.Id("txtPass"));
            pass.SendKeys("Smartpeg20");
            IWebElement login = driver.FindElement(By.Id("btnLogin"));
            login.Click();
            //anagrafica modifica volontarioto apri
            driver.FindElement(By.Id("ctl00_cphGuruMaster_272")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[6]/span/span")).Click();
            Thread.Sleep(2000);

            try
            {
                //elimina
                driver.FindElement(By.Id("ctl00_cphGuruMaster_rptCurriculumEx_ctl01_btnModifica")).Click();
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnElimina")).Click();
                Thread.Sleep(2000);
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
                Thread.Sleep(2000);
                //nuovo
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnNuovo")).Click();
                Thread.Sleep(2000);
                //modifica
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataInizio")).SendKeys("10 / 09 / 2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataFine")).SendKeys("20/09/2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAzienda")).SendKeys("Azienda");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoSettoreAzienda"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtCitta")).SendKeys("Perugia");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaese"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtMansioni")).SendKeys("aaa");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAnnotazioni")).SendKeys("bbb");
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnSalva")).Click();
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnIndietro")).Click();
            }
            catch
            {
                //nuovo
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnNuovo")).Click();
                Thread.Sleep(2000);
                //modifica
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataInizio")).SendKeys("10 / 09 / 2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataFine")).SendKeys("20/09/2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAzienda")).SendKeys("Azienda");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoSettoreAzienda"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtCitta")).SendKeys("Perugia");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaese"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtMansioni")).SendKeys("aaa");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAnnotazioni")).SendKeys("bbb");
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnSalva")).Click();
                //elimina
                driver.FindElement(By.Id("ctl00_cphGuruMaster_btnElimina")).Click();
                Thread.Sleep(2000);
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
                Thread.Sleep(2000);
                }
            }
        }
    }

