﻿using System;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
namespace LoginGuru_pt1
{
    class Program
    {
        private static IWebDriver Driver;

        static void Main(string[] args)
        {
            Driver = new ChromeDriver(Variables.WebDriverPath);
            Driver.Manage().Window.Maximize();

            // Tasks
            login();
            firstQuest();

            try
            {
                deleteElement();
                newElement();
                editElement();
            }
            catch
            {
                Console.WriteLine("There Was An Error!");
                newElement();
                editElement();
                deleteElement();
            }

            Console.ReadLine();
        }
        static void login()
        {
            Driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");

            // Define elements in webpage
            var Username = Driver.FindElement(By.Name("txtUser"));
            var Password = Driver.FindElement(By.Name("txtPass"));
            var Btn = Driver.FindElement(By.Name("btnLogin"));

            // Input text, password and click login
            Username.SendKeys("g.suriani");
            Password.SendKeys("Smartpeg20");
            Btn.Click();
        }

        static void firstQuest()
        {
            clickOn(By.Id("ctl00_cphGuruMaster_272"));
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit"));
            Thread.Sleep(Variables.defaultTimeout);
            ClearOn(By.Id("ctl00_cphGuruMaster_txtTelefono"));
            typeOn(By.Id("ctl00_cphGuruMaster_txtTelefono"), "123 456 7890");
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaeseRecapito"))).SelectByIndex(new Random().Next(1, 12));
            //clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
            IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[2]/span/span/input"));
            Thread.Sleep(Variables.defaultTimeout);

        }

        static void newElement()
        {
            int date = new Random().Next(1, 31);
            int month = new Random().Next(6, 12);
            int year = new Random().Next(2025, 2070);
            Thread.Sleep(Variables.defaultTimeout);
            try
            {
                clickOn(By.XPath("//table[@class='Tb']//input[@value='Nuovo']"));
            }
            catch
            {
                clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_elenco_gvRapporti_ctl01_btnNuovoEmpty"));
            }

            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtMatricola"), RandomString(4));
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_ddlTipoRapporto"))).SelectByIndex(5);
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtDataInizio"), "27/06/2007");
            typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtDataFine"), date + "/" + month + "/" + year);
            clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnSalva"));
            Thread.Sleep(Variables.defaultTimeout);

        }

        static void editElement()
        {
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div/div/div[4]/table/tbody/tr[2]/td[1]/input[1]"));
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtAnnotazioni"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnSalva"));
        }

        static void deleteElement()
        {
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div/div/div[4]/table/tbody/tr[2]/td[1]/input[1]"));
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnElimina"));
            IAlert alert = Driver.SwitchTo().Alert();
            alert.Accept();
        }




        // Utils


        // Random string
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        static void clickOn(By args)
        {
            Driver.FindElement(args).Click();
        }

        static void typeOn(By args, string typeElement)
        {
            Driver.FindElement(args).SendKeys(typeElement);
        }

        static void ClearOn(By args)
        {
            Driver.FindElement(args).Clear();
        }

    }

}
